import cv2
import numpy as np
import mediapipe as mp

from imageGetter import ImageGetter
from position import Position


class AirGraph():
    def __init__(self):
        self.__show_hands = True
        self.__show_phalanx = True
        self.__palette = cv2.imread('palette.png', cv2.IMREAD_COLOR)

        self.__frame = None
        self.__imageGetter = ImageGetter()
        self.__imageGetter.start()

        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_hands = mp.solutions.hands
        self.hands = self.mp_hands.Hands(min_detection_confidence=0.8, min_tracking_confidence=0.8)

        # Array of selected color for each hand
        self.__colors = []

        # Array of tip of index finger positions
        # Used to draw lines between previous position and current one for each hand.
        self.__positions = []

        cv2.namedWindow("AirGraph v0.1", cv2.WINDOW_NORMAL)
        cv2.namedWindow("Board", cv2.WINDOW_NORMAL)

        # Getting the size of the camera images
        success = False
        while not success:
            success, frame = self.imageGetter.frame
        self.shape = frame.shape
        print(self.shape)
        self.__board = np.zeros(self.shape)
        self.__drawing = np.zeros(self.shape)
        self.__palette = cv2.resize(self.__palette, [self.shape[1] // 10, self.shape[0]])
        self.__palette = self.__palette.astype(float) / 255

    @property
    def colors(self):
        return self.__colors

    @colors.setter
    def colors(self, value):
        self.__colors = value

    @property
    def palette(self):
        return self.__palette

    @property
    def positions(self):
        return self.__positions

    @positions.setter
    def positions(self, value):
        self.__positions = value

    @property
    def frame(self):
        return self.__frame

    @frame.setter
    def frame(self, value):
        self.__frame = value

    @property
    def imageGetter(self):
        return self.__imageGetter

    @imageGetter.setter
    def imageGetter(self, value):
        self.__imageGetter = value

    @property
    def board(self):
        return self.__board

    @board.setter
    def board(self, value):
        try:
            if value == 0:
                self.__board = np.zeros(self.shape)
        except ValueError:
            self.__board = value

    @property
    def drawing(self):
        return self.__drawing.copy()

    @drawing.setter
    def drawing(self, value):
        if value == 0:
            self.__drawing = np.zeros(self.shape)
        else:
            self.__drawing = value

    def run(self):
        while True:
            # Get the current image
            success, self.frame = self.imageGetter.frame
            if success:
                # Get hands position
                results = self.hands.process(cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB))
                # Reset board to black image
                self.board = 0

                # Hands has been detected
                if results.multi_hand_landmarks:

                    # Go through all the detected hands
                    hand_id = 0
                    for hand_landmarks in results.multi_hand_landmarks:
                        self.show_hands(hand_landmarks, hand_id)
                        if len(self.positions) <= hand_id:
                            self.positions.append(Position())

                        # Index 20 is the tip of the pinky finger
                        self.select_color(hand_landmarks.landmark[20], hand_id)

                        # Index 8 is the tip of the index finger.
                        # If index 8.y is under index 6.y (so > because (0,0) is on top left),
                        # that means that the hand is closed, so we don't draw
                        if hand_landmarks.landmark[8].y < hand_landmarks.landmark[6].y:
                            self.draw(hand_landmarks.landmark[8], hand_id)
                        else:
                            self.positions[hand_id].x = None
                            self.positions[hand_id].y = None

                        hand_id += 1
                else:
                    self.positions = []

                self.board = np.add(self.board, self.__drawing)

                self.board[self.shape[0] - self.palette.shape[0]:,
                self.shape[1] - self.palette.shape[1]:] = self.palette
                self.show()

            # Goodbye !
            key = cv2.waitKey(1)
            if key & 0xFF == 27:
                self.imageGetter.capture = False
                break
            elif key == ord("s"):
                img = np.float32(self.drawing) * 255
                cv2.imwrite("img.jpg", img)
                print('Image saved')

    def select_color(self, pinky_finger, hand_id):
        if len(self.colors) <= hand_id:
            self.colors.append((100, 150, 200))

        # If pinky finger is on the palette
        if pinky_finger.x > 0.9:
            color = self.palette[int(pinky_finger.y * self.shape[0])][0]
            color = color.astype(float)
            self.colors[hand_id] = tuple([c for c in color])

    def show_hands(self, hand_landmarks, hand_id):
        if self.__show_hands:
            self.mp_drawing.draw_landmarks(self.board, hand_landmarks, self.mp_hands.HAND_CONNECTIONS)

        if self.__show_phalanx:
            counter = 0
            for landmark in hand_landmarks.landmark:
                if counter == 0:
                    text = "{}-{}".format(hand_id, counter)
                else:
                    text = "{}".format(counter)
                x = int(landmark.x * self.shape[1])
                y = int(landmark.y * self.shape[0])
                cv2.putText(self.board, text, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
                counter += 1

    def draw(self, landmark, hand_id):
        # We don't want to draw over the palette !
        if landmark.x < 0.9:
            x = int(landmark.x * self.shape[1])
            y = int(landmark.y * self.shape[0])
            color = self.colors[hand_id]
            if self.positions[hand_id].x is not None and self.positions[hand_id].y is not None:
                cv2.line(self.__drawing, (self.positions[hand_id].x, self.positions[hand_id].y), (x, y),
                         tuple(color), 6, cv2.LINE_AA)

            self.positions[hand_id].x = x
            self.positions[hand_id].y = y

    def show(self):
        cv2.imshow("AirGraph v0.1", self.frame)
        cv2.imshow("Board", self.board)


if __name__ == '__main__':
    airGraph = AirGraph()
    airGraph.run()
