from time import sleep

import cv2
from threading import Thread, Lock

class ImageGetter(Thread):
    def __init__(self, path=0):
        Thread.__init__(self)
        self.__stream = cv2.VideoCapture(path)
        self.__frame = None
        self.__capture = True
        self.__frameMutex = Lock()

    @property
    def frame(self):
        self.__frameMutex.acquire()
        try:
            currentFrame = self.__frame.copy()
            success = True
        except AttributeError:
            currentFrame = None
            success = False
        self.__frameMutex.release()
        return [success, currentFrame]

    @frame.setter
    def frame(self, value):
        self.__frameMutex.acquire()
        self.__frame = value
        self.__frameMutex.release()

    @property
    def capture(self):
        return self.__capture

    @capture.setter
    def capture(self, value):
        self.__capture = value

    def run(self):
        while self.__stream.isOpened() and self.capture:
            success, frame = self.__stream.read()
            if success:
                # Flip the image horizontally for a later selfie-view display, and convert
                # the BGR image to RGB.
                #frame = cv2.cvtColor(cv2.flip(frame, 1), cv2.COLOR_BGR2RGB)
                frame = cv2.flip(frame, 1)
                # To improve performance, optionally mark the image as not writeable to
                # pass by reference.
                frame.flags.writeable = False

                self.frame = frame
            sleep(0.05)
        print("Capture finished")