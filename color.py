class Col:
    DEFAULT = [0, 152, 150]

    def __init__(self, color=DEFAULT):
        self.__red = color[0]
        self.__green = color[1]
        self.__blue = color[2]

    def totuple(self):
        return tuple([self.red, self.green, self.blue])

    def __repr__(self):
        return "RGB: ({}, {}, {})".format(self.red, self.green, self.blue)

    @property
    def red(self):
        return self.__red

    @red.setter
    def red(self, value):
        self.__red = value

    @property
    def green(self):
        return self.__green

    @green.setter
    def green(self, value):
        self.__green = value

    @property
    def blue(self):
        return self.__blue

    @blue.setter
    def blue(self, value):
        self.__blue = value
