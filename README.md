# AirGraph - POC

> This is a POC, don't mind about code cleanliness !

## Install

Use Python 3.9 with the following packages

- mediapipe
- numpy
- opencv

```sh
$ pip install -r requirements.txt
```

## Usage

```sh
$ python airGraph.py
```

Use your index finger to draw things. When the finger is up, it draws. When it's down, it stop drawing.

Use your pinky finger to select colors on the right side.
